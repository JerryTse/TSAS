var should = require('Should');
global.request = require('supertest');
request = request('http://billfyp.myds.me:8083');
describe('Job API,', () => {
    it('I am student user, I can create a job.', (done) => {
        let headers = { "Authorization": "Basic SmVycnkgVHNlOjEyMzQ1Ng==", "Content-Type": "application/json" };
        let body = {
            "subject": "Secondary 5 English",
            "numberOfStudents": 1,
            "location": "138 Bonham Strand, Sheung Wan, Hong Kong, Hong Kong SAR",
            "relatedStudentUser": "5ae85b863aacf800c75ae241"
        };

        request.post('/jobs')
            .set(headers)
            .send(body)
            .end((err, res) => {
                res.status.should.be.equal(201);
                done();
            });
    }).timeout(10000);

    it('I am student user, I can update a job.', (done) => {
        let headers = { "Authorization": "Basic SmVycnkgVHNlOjEyMzQ1Ng==", "Content-Type": "application/json" };
        let body = {
            "subject": "Secondary 4 English"
        };

        request.put('/jobs/5ae837233aacf800c75ae203')
            .set(headers)
            .send(body)
            .end((err, res) => {
                res.status.should.be.equal(200);
                done();
            });
    }).timeout(10000);

    it('I am student user, I can delete a job.', (done) => {
        let headers = { "Authorization": "Basic SmVycnkgVHNlOjEyMzQ1Ng=="};

        request.delete('/jobs/5ae878110fe3b800d0068e44')
            .set(headers)
            .end((err, res) => {
                res.status.should.be.equal(204);
                done();
            });
    }).timeout(10000);

    it('I am tutor user, I can accept a job.', (done) => {
        let headers = { "Authorization": "Basic bWlzc2xlZToxMjM0NTY=", "Content-Type": "application/json" };
        let body = {
            "relatedTutorUser": "5ae860553aacf800c75ae256"
        };

        request.put('/jobs/5ae8613e3aacf800c75ae259')
            .set(headers)
            .send(body)
            .end((err, res) => {
                res.status.should.be.equal(200);
                done();
            });
    }).timeout(10000);
});