var should = require('Should');
global.request = require('supertest');
request = request('http://billfyp.myds.me:8083');
describe('User API (Register),', () => {
    it('I can register a new user account.', (done) => {
        let headers = { "Content-Type": "application/json" }
        let body = {
            "username": "Jerry Tse",
            "email": "tkc1994125@gmail.com",
            "password": "123456",
            "userType": "Student"
        };

        request.post('/users')
            .set(headers)
            .send(body)
            .end((err, res) => {
                res.status.should.be.equal(201);
                done();
            });
    }).timeout(10000);

    it('I cannot register if username is empty.', (done) => {
        let headers = { "Content-Type": "application/json" }
        let body = {
            "email": "tkc1994125@gmail.com",
            "password": "123456",
            "userType": "Student"
        };

        request.post('/users')
            .set(headers)
            .send(body)
            .end((err, res) => {
                res.status.should.be.equal(400);
                res.body.message.should.be.equal("Username cannot be empty")
                done();
            });
    }).timeout(10000);

    it('I cannot register if password is empty.', (done) => {
        let headers = { "Content-Type": "application/json" }
        let body = {
            "username": "Jerry Tse",
            "email": "tkc1994125@gmail.com",
            "userType": "Student"
        };

        request.post('/users')
            .set(headers)
            .send(body)
            .end((err, res) => {
                res.status.should.be.equal(400);
                res.body.message.should.be.equal("Password cannot be empty")
                done();
            });
    }).timeout(10000);

    it('I can update a user account.', (done) => {
        let headers = { "Content-Type": "application/json" }
        let body = {
            "username": "Jerry Tse",
            "email": "tkc1994125@gmail.com",
            "password": "123456",
            "userType": "Student"
        };

        request.put('/users')
            .set(headers)
            .send(body)
            .end((err, res) => {
                res.status.should.be.equal(405);
                done();
            });
    }).timeout(10000);

    it('I cannot register if user is exist.', (done) => {
        let headers = { "Content-Type": "application/json" }
        let body = {
            "username": "Jerry",
            "email": "tkc1994125@gmail.com",
            "password": "123456",
            "userType": "Student"
        };

        request.put('/users')
            .set(headers)
            .send(body)
            .end((err, res) => {
                res.status.should.be.equal(405);
                done();
            });
    }).timeout(10000);

    it('I am registered user and i can login and get user info.', (done) => {
        let headers = { "Authorization": "Basic SmVycnkgVHNlOjEyMzQ1Ng==", "Content-Type": "application/json" };
        let body = {};

        request.post('/login')
            .set(headers)
            .send(body)
            .end((err, res) => {
                res.should.be.an.Object();
                res.status.should.be.equal(200);
                res.body.should.be.an.Object();
                done();
            });
    }).timeout(10000);
});