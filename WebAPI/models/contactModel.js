const mongoose = require('mongoose');
const mongooseStringQuery = require('mongoose-string-query');
Schema = mongoose.Schema;
var UserModel = require('../models/userModel');


var contactSchema = new mongoose.Schema({
    sender: { type: Schema.Types.ObjectId, ref: UserModel },
    senderName: String,
    receiver: { type: Schema.Types.ObjectId, ref: UserModel },
    receiverName: String,
    messageBody: String,
    status: {
        type: String,
        default: 'Sent',
        enum: ['Sent', 'Read']
    },
    createdDateTime: Date
});

contactSchema.plugin(mongooseStringQuery);

module.exports = mongoose.model('contact', contactSchema);