const mongoose = require('mongoose');
const mongooseStringQuery = require('mongoose-string-query');
Schema = mongoose.Schema;
var JobModel = require('../models/jobModel');

var userSchema = new mongoose.Schema({
    userType: {
        type: String,
        enum: ['Student', 'Tutor', 'Admin']
    },
    username: String,
    englishName: String,
    chineseName: String,
    password: { type: String, required: true },
    mobileNo: String,
    contactNo: String,
    Email: String,
    residenceDistrict: String,
    residenceAddress: String,

    relationshipWithStudent: String,

    gender: String,
    dateOfBirth: Date,
    mainLanguage: String,
    religion: String,
    sexualOffenses: Boolean,
    highestLevelOfEducation: String,
    primarySchool: String,
    secondarySchool: String,
    university: String,
    collegeMajors: String,
    otherProfessionals: String,
    currentlyAttendingGrade: String,
    hongKongCertificateOfEducationExaminationScore: Number,
    yearsOfExperienceAndLessons: Number,
    selfIntroduction: String,

    status: {
        type: String,
        enum: ['New', 'Approved', 'Banned']
    },
    
    tutorCategory: [{
        categories: String,
        levels: String,
        subjects: String
    }],
    areaOfLesson: [{
        area: String
    }],
    ratings: [{
        _id: { type: Schema.Types.ObjectId, ref: this }, username: String, score: Number, comment: String
    }]
});

userSchema.plugin(mongooseStringQuery);

module.exports = mongoose.model('user', userSchema);