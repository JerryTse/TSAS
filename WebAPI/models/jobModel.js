const mongoose = require('mongoose');
const mongooseStringQuery = require('mongoose-string-query');
Schema = mongoose.Schema;
var UserModel = require('../models/userModel');

var jobSchema = new mongoose.Schema({
    district: String,
    location: String,
    lat: Number,
    lng: Number,
    categories: String,
    levels: String,
    subject: String,
    tuitionFeePerUnit: Number,
    tuitionUnit: String,
    numberOfStudents: Number,
    tutorAcademicRequirements: String,
    tutorGenderRequest: String,
    numberOfTimesPerWeek: String,
    lessonDuration: {
        hour: Number,
        minute: Number
    },
    lessonTime: String,
    createdDateTime: Date,
    contactNumber: String,
    remark: String,
    status: { type: String, default: 'Initial' },

    interestedTutor: [{
        _id: { type: Schema.Types.ObjectId, ref: UserModel }, tutorName: String
    }],
    relatedStudentUser: { type: Schema.Types.ObjectId, ref: UserModel },
    relatedTutorUser: { type: Schema.Types.ObjectId, ref: UserModel }
});

jobSchema.plugin(mongooseStringQuery);

module.exports = mongoose.model('job', jobSchema);