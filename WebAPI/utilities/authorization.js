const User = require('../models/userModel');

// api authorization
module.exports = function (req) {
    const auth = req.authorization;

    return new Promise((resolve, reject) => {
        if (!auth.basic.username || !auth.basic.password) {
            return reject({ code: 401, response: { status: 'error', message: 'Missing username or password' } })
        }

        User.findOne({ username: auth.basic.username, password: auth.basic.password }, function (err, user) {
            if (err) {  // Server error
                console.log(err);
                reject({ message: err });
            }

            if (!user) {   //not found
                reject({ message: "Authorization Failed" });
            }

            resolve();
        });
    });
};