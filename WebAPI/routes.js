﻿module.exports = function (app) {
    require('./routes/userRoutes.js')(app);
    require('./routes/jobRoutes.js')(app);
    require('./routes/contactRoutes')(app);
}