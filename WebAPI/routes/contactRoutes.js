const errors = require('restify-errors');
const Contact = require('../models/contactModel');
const checkUserExist = require('../utilities/authorization');
var request = require("request");

module.exports = function (server) {
    // Create message
    server.post('/contacts', (req, res, next) => {
        checkUserExist(req).then(function () {

            if (!req.is('application/json')) {
                return next(
                    new errors.InvalidContentError("Expects 'application/json'")
                );
            }

            var data = req.body || {};

            var contact = new Contact(data);
            contact.save(function (err) {
                if (err) {
                    console.error(err);
                    return next(new errors.InternalError(err.message));
                    next();
                }

                res.send(201);
                next();
            });

        }).catch(function (msg) {
            return res.send(403, { message: msg });
        });
    });

    // get all related messages
    server.get('/contacts/:senderid/:receiverid', (req, res, next) => {
        checkUserExist(req).then(function () {
            Contact.find({
                "sender": { $in: [req.params.senderid, req.params.receiverid] }, "receiver": { $in: [req.params.senderid, req.params.receiverid] }
            }, function (err, messages) {
                if (err) {
                    console.error(err);
                    return next(
                        new errors.InvalidContentError(err.errors.name.message)
                    );
                }

                res.send(messages);
                next();
            });
        }).catch(function (msg) {
            return res.send(403, { message: msg });
        });
    });

    // get all messages of a user
    server.get('/contacts/:userid', (req, res, next) => {
        checkUserExist(req).then(function () {
            Contact.find({ $or: [{ "sender": req.params.userid }, { "receiver": req.params.userid }] }, function (err, messages) {
                if (err) {
                    console.error(err);
                    return next(
                        new errors.InvalidContentError(err.errors.name.message)
                    );
                }

                res.send(messages);
                next();
            });
        }).catch(function (msg) {
            return res.send(403, { message: msg });
        });
    });
}