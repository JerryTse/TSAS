const errors = require('restify-errors');
const User = require('../models/userModel');
const checkUserExist = require('../utilities/authorization');

module.exports = function (server) {
    // create user
    server.post('/users', (req, res, next) => {
        if (!req.is('application/json')) {
            return next(
                new errors.InvalidContentError("Expects 'application/json'")
            );
        }

        if (!req.body.username) {
            return res.send(400, { message: "Username cannot be empty" });
        }

        if (!req.body.password) {
            return res.send(400, { message: "Password cannot be empty" });
        }

        var data = req.body || {};

        var user = new User(data);
        user.save(function (err) {
            if (err) {
                console.error(err);
                return next(new errors.InternalError(err.message));
                next();
            }

            res.send(201);
            next();
        });
    });

    // get all users
    server.get('/users', (req, res, next) => {
        checkUserExist(req).then(function () {
            User.apiQuery(req.params, function (err, users) {
                if (err) {
                    console.error(err);
                    return next(
                        new errors.InvalidContentError(err.errors.name.message)
                    );
                }

                res.send(users);
                next();
            });
        }).catch(function (msg) {
            return res.send(403, { message: msg });
        });
    });

    // get all tutor users
    server.get('/users/tutors', (req, res, next) => {
        User.find({ "userType": "Tutor" }, function (err, users) {
            if (err) {
                console.error(err);
                return next(
                    new errors.InvalidContentError(err.errors.name.message)
                );
            }

            res.send(users);
            next();
        });
    });

    server.post('/users/tutors', (req, res, next) => {
        if (!req.is('application/json')) {
            return next(
                new errors.InvalidContentError("Expects 'application/json'")
            );
        }

        var query = {
            userType: "Tutor",
            gender: req.body.gender,
            status: "Approved",
            "tutorCategory.subjects": { $in: ["All", req.body.subject] }
        };

        User.find(query, function (err, users) {
            if (err) {
                console.error(err);
                return next(
                    new errors.InvalidContentError(err.errors.name.message)
                );
            }

            res.send(users);
            next();
        });
    });

    // get a user details
    server.get('/users/:userId', (req, res, next) => {
        checkUserExist(req).then(function () {
            User.findOne({ _id: req.params.userId }, function (err, user) {
                if (err) {
                    console.error(err);
                    return next(
                        new errors.InvalidContentError(err.errors.name.message)
                    );
                }

                if (!user) {
                    return res.send(404, { message: "User not found with id " + req.params.userId });
                }

                res.send(user);
                next();
            });
        }).catch(function (msg) {
            return res.send(403, { message: msg });
        });
    });

    // user login
    server.post('/login', (req, res, next) => {
        const auth = req.authorization

        if (!auth.basic.username) {
            return res.send(400, { message: "Username cannot be empty" });
        }

        if (!auth.basic.password) {
            return res.send(400, { message: "Password cannot be empty" });
        }

        User.findOne({ username: auth.basic.username, password: auth.basic.password }, function (err, user) {
            if (err) {
                console.error(err);
                return next(
                    new errors.InvalidContentError(err.errors.name.message)
                );
            }

            if (!user) {
                return res.send(404, { message: "Username or password not match." });
            }

            res.send(user);
            next();
        });
    });

    //check user exist
    server.get('/users/check/:username', (req, res, next) => {
        User.findOne({ username: req.params.username }, function (err, user) {
            if (err) {
                console.error(err);
                return next(
                    new errors.InvalidContentError(err.errors.name.message)
                );
            }

            if (!user) {
                return res.send(200, { message: "User not found" });
            }

            return res.send(409, { message: "User already exist" });
        });
    });

    // update a user
    server.put('/users/:userId', (req, res, next) => {
        checkUserExist(req).then(function () {
            if (!req.is('application/json')) {
                return next(
                    new errors.InvalidContentError("Expects 'application/json'")
                );
            }

            var data = req.body || {};

            if (!data._id) {
                data = Object.assign({}, data, { _id: req.params.userId });
            }

            User.findOne({ _id: req.params.userId }, function (err, user) {
                if (err) {
                    console.error(err);
                    return next(
                        new errors.InvalidContentError(err.errors.name.message)
                    );
                } else if (!user) {
                    return next(
                        new errors.ResourceNotFoundError(
                            'The resource you requested could not be found.'
                        )
                    );
                }

                User.update({ _id: data._id }, data, function (err) {
                    if (err) {
                        console.error(err);
                        return next(
                            new errors.InvalidContentError(err.errors.name.message)
                        );
                    }

                    res.send(200, data);
                    next();
                });
            });
        }).catch(function (msg) {
            return res.send(403, { message: msg });
        });
    });

    // delete a user
    server.del('/users/:userId', (req, res, next) => {
        checkUserExist(req).then(function () {
        User.remove({ _id: req.params.userId }, function (err) {
            if (err) {
                console.error(err);
                return next(
                    new errors.InvalidContentError(err.errors.name.message)
                );
            }

            res.send(204);
            next();
            });
        }).catch(function (msg) {
            return res.send(403, { message: msg });
        });
    });
}