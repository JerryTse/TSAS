const errors = require('restify-errors');
const User = require('../models/userModel');
const Job = require('../models/jobModel');
const checkUserExist = require('../utilities/authorization');
var request = require("request");

module.exports = function (server) {
    // parents or student create a job
    server.post('/jobs', (req, res, next) => {
        checkUserExist(req).then(function () {
            if (!req.is('application/json')) {
                return next(
                    new errors.InvalidContentError("Expects 'application/json'")
                );
            }

            var data = req.body || {};

            User.findOne({ _id: data.relatedStudentUser }, function (err, user) {
                if (err) {
                    console.error(err);
                    return next(
                        new errors.InvalidContentError(err.errors.name.message)
                    );
                } else if (!user) {
                    return next(
                        new errors.ResourceNotFoundError(
                            'The resource you requested could not be found.'
                        )
                    );
                };

                var job = new Job(data);
                job.relatedStudentUser = user;

                if (data.location) {
                    new Promise((resolve, reject) => {
                        request({
                            method: 'GET',
                            url: 'https://maps.googleapis.com/maps/api/geocode/json',
                            qs: {
                                address: data.location,
                                key: 'AIzaSyD7u07UY3zbgrzJZAj6kkNgfMlvFKQVXyo' // Need to get API Key From Google
                            },
                        }, function (error, response, body) {
                            if (error) {
                                reject(error);
                            }
                            if (body) {
                                resolve(body);
                            }
                        });
                    }).then(function (body) {
                        result = JSON.parse(body);
                        job.lat = result.results[0].geometry.location.lat;
                        job.lng = result.results[0].geometry.location.lng;
                        job.save(function (err) {
                            if (err) {
                                console.error(err);
                                return next(new errors.InternalError(err.message));
                                next();
                            }

                            res.send(201);
                            next();
                        })
                    }).catch(function (error) {
                        throw new Error(error);
                    });
                } else {
                    return res.send(400, { message: "Location cannot be empty" });
                }
            });
        }).catch(function (msg) {
            return res.send(403, { message: msg });
        });
    });
    
    // get all jobs of student user
    server.get('/jobs/student/:userId', (req, res, next) => {
        checkUserExist(req).then(function () {
            Job.find({ 'relatedStudentUser': req.params.userId }, function (err, jobs) {
                if (err) {
                    console.error(err);
                    return next(
                        new errors.InvalidContentError(err.errors.name.message)
                    );
                }

                res.send(jobs);
                next();
            });
        }).catch(function (msg) {
            return res.send(403, { message: msg });
        });
    });

    // get all jobs of tutor user
    server.get('/jobs/tutor/:userId', (req, res, next) => {
        checkUserExist(req).then(function () {
            Job.find({ 'relatedTutorUser': req.params.userId }, function (err, jobs) {
                if (err) {
                    console.error(err);
                    return next(
                        new errors.InvalidContentError(err.errors.name.message)
                    );
                }

                res.send(jobs);
                next();
            });
        }).catch(function (msg) {
            return res.send(403, { message: msg });
        });
    });

    // get all not accepted job
    server.post('/joblist', (req, res, next) => {
        if (!req.is('application/json')) {
            return next(
                new errors.InvalidContentError("Expects 'application/json'")
            );
        }

        var query = { relatedTutorUser: null };
        if (req.body.subject != '') {
            var getValue = req.body.subject;
            var regexValue = '\.*' + getValue + '\.*';
            query = { subject: new RegExp(regexValue, 'i'), relatedTutorUser: null };
        } else if (req.body.level != '') {
            var getValue = req.body.level;
            var regexValue = '\.*' + getValue + '\.*';
            query = { levels: new RegExp(regexValue, 'i'), relatedTutorUser: null };
        } else if (req.body.subject != '' && req.body.level != '') {
            var getValue = req.body.subject;
            var regexValue = '\.*' + getValue + '\.*';
            var getValue1 = req.body.level;
            var regexValue1 = '\.*' + getValue1 + '\.*';
            query = { levels: new RegExp(regexValue1, 'i'), subject: new RegExp(regexValue, 'i'), relatedTutorUser: null };
        }
        
        Job.find(query, function (err, jobs) {
            if (err) {
                console.error(err);
                return next(
                    new errors.InvalidContentError(err.errors.name.message)
                );
            }

            res.send(jobs);
            next();
        });
    });

    // get a job details
    server.get('/jobs/job/:jobId', (req, res, next) => {
        Job.findOne({ _id: req.params.jobId }, function (err, job) {
            if (err) {
                console.error(err);
                return next(
                    new errors.InvalidContentError(err.errors.name.message)
                );
            }

            if (!job) {
                return res.send(404, { message: "Job not found with id " + req.params.jobId });
            }

            res.send(job);
            next();
        });
    });

    // update a job
    server.put('/jobs/:jobId', (req, res, next) => {
        checkUserExist(req).then(function () {
            if (!req.is('application/json')) {
                return next(
                    new errors.InvalidContentError("Expects 'application/json'")
                );
            }

            var data = req.body || {};

            Job.findOne({ _id: req.params.jobId }, function (err, job) {
                if (err) {
                    console.error(err);
                    return next(
                        new errors.InvalidContentError(err.errors.name.message)
                    );
                } else if (!job) {
                    return next(
                        new errors.ResourceNotFoundError(
                            'The resource you requested could not be found.'
                        )
                    );
                };

                // if relatedTutorUser is not empty, tutor accept job
                // else normal update a job
                if (data.relatedTutorUser) {
                    User.findOne({ _id: data.relatedTutorUser }, function (err, user) {
                        if (err) {
                            console.error(err);
                            return next(
                                new errors.InvalidContentError(err.errors.name.message)
                            );
                        } else if (!user) {
                            return next(
                                new errors.ResourceNotFoundError(
                                    'The resource you requested could not be found.'
                                )
                            );
                        };

                        if (user.userType != "Tutor") {
                            return res.send(400, { message: "User is not Tutor" });
                        }

                        Job.update({ _id: req.params.jobId }, { $set: { "relatedTutorUser": user._id } }, function (err) {
                            if (err) {
                                console.error(err);
                                return next(
                                    new errors.InvalidContentError(err.errors.name.message)
                                );
                            }

                            res.send(200, data);
                            next();
                        });
                    });
                } else {
                    Job.update({ _id: req.params.jobId }, data, function (err) {
                        if (err) {
                            console.error(err);
                            return next(
                                new errors.InvalidContentError(err.errors.name.message)
                            );
                        }

                        res.send(200, data);
                        next();
                    });
                };
            });
        }).catch(function (msg) {
            return res.send(403, { message: msg });
        });
    });

    // delete a job
    server.del('/jobs/:jobId', (req, res, next) => {
        checkUserExist(req).then(function () {
            Job.findOne({ _id: req.params.jobId }, function (err, job) {
                if (err) {
                    console.error(err);
                    return next(
                        new errors.InvalidContentError(err.errors.name.message)
                    );
                }

                if (!job) {
                    return res.send(400, { message: "Job not found with id " + req.params.jobId });
                }

                if (job.relatedTutorUser) {
                    return res.send(400, { message: "Cannot delete this job because a tutor is accepted the job and not in Completed status" });
                }

                Job.remove({ _id: req.params.jobId }, function (err) {
                    if (err) {
                        console.error(err);
                        return next(
                            new errors.InvalidContentError(err.errors.name.message)
                        );
                    }

                    res.send(204);
                    next();
                });
            });
        }).catch(function (msg) {
            return res.send(403, { message: msg });
        });
    });
}